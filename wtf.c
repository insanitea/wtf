#include <limits.h>
#include <poll.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/inotify.h>
#include <sys/wait.h>
#include <termios.h>
#include <unistd.h>

#define MULTI_ARG_TERM "--"
#define EVENT_SIZE(ev) (sizeof(struct inotify_event)+ev->len)
#define EVENT_POOL 8

#define printerr(...) fprintf(stderr, "\e[1;31merror\e[0;31m:\e[0m " __VA_ARGS__)

char *usage = "wtf <flags> [ arguments .. ] [ command .. ]\n"
"\n"
"  c  clear output before running command\n"
"  e  implicitly run command every timeout\n"
"  i  initially run command on start\n"
"  u  set terminal bell on match\n"
"  t  <timeout>\n"
"\n"
"  d  [ directories .. ] --\n"
"  f  [ filenames .. ] --\n"
"  s  [ substrings .. ] --\n"
"\n";

pid_t pid = 0;
char **cmdv = NULL,
     **dirs = NULL,
     **files = NULL,
     **substrs = NULL;
size_t n_dirs = 0,
       n_files = 0,
       n_substrs = 0;
bool auto_clear = false,
     implicit_run = false,
     initial_run = false,
     term_bell = false;
unsigned int timeout = 2500;

void clear(void) {
    if(pid != 0)
        return;

    fwrite("\ec", 1, 2, stderr);
    fflush(stderr);
}

void run(void) {
    if(pid != 0)
        return;

    if(auto_clear) {
        fwrite("\ec", 1, 2, stderr);
        fflush(stderr);
    }

    pid = fork();
    if(pid == 0) {
        if(execvp(cmdv[0], cmdv) != 0) {
            printerr("cannot run specified command\n");
            exit(1);
        }
    }
}

void stop(void) {
    if(pid != 0)
        kill(pid, SIGTERM);
}

void sigchld(int sig) {
    while(wait(NULL) > 0);
    pid = 0;
}

bool match_file(char *name) {
    size_t i;

    for(i=0; i < n_files; ++i) {
        if(strcmp(name, files[i]) == 0)
            return true;
    }

    for(i=0; i < n_substrs; ++i) {
        if(strstr(name, substrs[i]) != NULL)
            return true;
    }

    return false;
}

struct ev_buffer {
    char *data;
    size_t len,
           rem,
           max;
};

int buf_init(struct ev_buffer *buf, size_t max) {
    char *data;

    data = malloc(max);
    if(data == NULL)
        return -1;

    memset(data, 0, max);

    buf->data = data;
    buf->len = 0;
    buf->rem = max;
    buf->max = max;

    return 0;
}

void buf_deinit(struct ev_buffer *buf) {
    if(buf->data != NULL)
        free(buf->data);

    memset(buf, 0, sizeof(struct ev_buffer));
}

ssize_t buf_fill(struct ev_buffer *buf, int fd) {
    ssize_t n;

    if(buf->rem == 0)
        return 0;

    n = read(fd, &buf->data[buf->len], buf->rem);
    if(n == -1) {
        printerr("read() failed\n");
        return -1;
    }

    buf->len += n;
    buf->rem -= n;

    return n;
}

ssize_t buf_spill(struct ev_buffer *buf, size_t n) {
    size_t r;

    if(n > buf->len)
        n = buf->len;

    r = buf->len-n;
    memmove(buf->data, buf->data+n, r);
    memset(buf->data+n, 0, r);

    buf->len = r;
    buf->rem = buf->max-r;

    return n;
}

void buf_debug(struct ev_buffer *buf) {
    fprintf(stderr, "+%zu/%zu rem=%zu\n", buf->len, buf->max, buf->rem);
}

int main(int argc, char **argv) {
    struct termios t;
    cc_t pvmin;
    struct sigaction sa;

    char *flags,
         cwd[PATH_MAX];
    struct ev_buffer buf;
    int fd,
        *dir_fds;
    size_t n_flags,
           i,j,ai,
           n_matches;
    struct pollfd p[2];
    struct inotify_event *ev;
    char c;

    if(argc == 1) {
        printf("%s", usage);
        return 1;
    }

    // parse flags and arguments

    flags = argv[1];
    n_flags = strlen(flags);

    for(i=0,ai=2; i < n_flags; ++i) {
        if(flags[i] == 'c') {
            auto_clear = true;
        }
        else if(flags[i] == 'e') {
            implicit_run = true;
        }
        else if(flags[i] == 'i') {
            initial_run = true;
        }
        else if(flags[i] == 'u') {
            term_bell = true;
        }
        else if(flags[i] == 't') {
            timeout = strtoull(argv[ai++], NULL, 0);
        }
        else if(flags[i] == 'd' && ai < argc) {
            dirs = &argv[ai];
            while(ai < argc) {
                if(strcmp(argv[ai++], MULTI_ARG_TERM) == 0)
                    break;

                ++n_dirs;
            }

            continue;
        }
        else if(flags[i] == 'f' && ai < argc) {
            files = &argv[ai];
            while(ai < argc) {
                if(strcmp(argv[ai++], MULTI_ARG_TERM) == 0)
                    break;

                ++n_files;
            }

            continue;
        }
        else if(flags[i] == 's' && ai < argc) {
            substrs = &argv[ai];
            while(ai < argc) {
                if(strcmp(argv[ai++], MULTI_ARG_TERM) == 0)
                    break;

                ++n_substrs;
            }

            continue;
        }
    }

    cmdv = &argv[ai];

    // disable echo

    tcgetattr(STDIN_FILENO, &t);
    pvmin = t.c_cc[VMIN];
    t.c_lflag &= ~(ECHO|ICANON);
    t.c_cc[VMIN] = 1;
    tcsetattr(STDIN_FILENO, TCSANOW, &t);

    // setup signal handlers

    sa.sa_handler = sigchld;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = 0;
    if(sigaction(SIGCHLD, &sa, 0) == -1) {
        printerr("sigaction() failed\n");
        exit(1);
    }

    // setup inotify

    if(getcwd(cwd, PATH_MAX) == NULL) {
        printerr("cannot get current working directory\n");
        return 1;
    }

    fd = inotify_init();
    if(fd < 0) {
        printerr("inotify_init() failed\n");
        return 1;
    }

    dir_fds = malloc(sizeof(int)*n_dirs);

    dir_fds[0] = inotify_add_watch(fd, cwd, IN_CLOSE_WRITE);
    if(dir_fds[0] == -1) {
        printerr("cannot add watch for current directory\n");
        return 1;
    }

    for(i=0,j=1; i < n_dirs; ++i,++j) {
        dir_fds[j] = inotify_add_watch(fd, dirs[i], IN_CLOSE_WRITE);
        if(dir_fds[j] == -1) {
            printerr("cannot watch directory \"%s\"\n", dirs[i]);
            continue;
        }
    }

    // event main loop

    if(buf_init(&buf, (sizeof(struct inotify_event)+NAME_MAX+1)*EVENT_POOL) == -1)
        return 1;

    p[0].fd = fd;
    p[0].events = POLLIN;
    p[1].fd = STDIN_FILENO;
    p[1].events = POLLIN;

    n_matches = 0;
    if(initial_run)
        run();

    ev = ((struct inotify_event *)buf.data);

    while(true) {
        if(poll(p, 2, timeout) == 0) {
            if(implicit_run || n_matches > 0) {
                n_matches = 0;
                run();
            }

            continue;
        }

        // handle file events

        if(p[0].revents & POLLIN) {
            if(buf_fill(&buf, fd) == -1)
                return -1;

            while(buf.len >= sizeof(struct inotify_event)) {
                if(match_file(ev->name)) {
                    if(++n_matches == 1) {
                        if(auto_clear)
                            fwrite("\ec", 1, 2, stderr);
                        if(term_bell)
                            fwrite("\a", 1, 1, stderr);
                    }

                    if(ev->len > 0)
                        fprintf(stderr, "\e[0;93mmodified\e[0;33m:\e[0m %s\n", ev->name);
                    else
                        fprintf(stderr, "\e[0;93mmodify\e[0m\n");
                }

                buf_spill(&buf, sizeof(struct inotify_event)+ev->len);
            }
        }

        // handle input events

        if(p[1].revents & POLLIN) {
            while(fread(&c, 1, 1, stdin) == 1) {
                if(c == 'c') {
                    clear();
                }
                else if(c == 'r') {
                    run();
                }
                else if(c == 's') {
                    stop();
                }
                else if(c == 'q') {
                    goto quit;
                }
            }
        }
    }

quit:

    // clean up

    t.c_lflag |= (ECHO|ICANON);
    t.c_cc[VMIN] = pvmin;
    tcsetattr(STDIN_FILENO, TCSANOW, &t);

    for(i=0; i < n_dirs; ++i)
        close(dir_fds[i]);

    close(fd);
    buf_deinit(&buf);
    free(dir_fds);

    return 0;
}
